# Important update

This repo is readonly and is here because it is mentioned in the article (https://doi.org/10.12688/f1000research.8192.2). I will keep it for historical and ethical reasons, but I personally am moving away from Bitbucket, so you can find a fork fo this repo in the following place:

<https://codeberg.org/mehrad/dream-prostate-cancer-challenge-q.1a>